(function($, Drupal) {
  Drupal.behaviors.shuryVideo = {
    attach: function(context, settings) {
      var video = $('#video', context);
      var figure = $('.full-height figure', context);

      //Video init
      if (video.length) {
        Drupal.shuryVideo.videoProportions(video, figure);
        $(window).once('HeroVideo').trigger('resize');
      }
    },
  };

  Drupal.shuryVideo = Drupal.shuryVideo || {};

  //Video proportions check
  Drupal.shuryVideo.videoProportions = function(video, figure) {
    Drupal.shuryVideo.proportionCheck(video);
    var resizeTimer;
    $(window).on('resize', function() {
      if (window.innerWidth >= 1024) {
        video.removeClass('hidden');
        figure.addClass('hidden');
        video.each(function(i, el) {
          el.controls = false;
          el.loop = true;
          el.muted = true;
          el.play();
        });
      }
      else {
        video.addClass('hidden');
        figure.removeClass('hidden');
      }
      resizeTimer = Drupal.shuryVideo.freezePlaying(video, resizeTimer);
      Drupal.shuryVideo.proportionCheck(video);
    });
  };

  // Video playing pausing.
  Drupal.shuryVideo.freezePlaying = function(video, resizeTimer) {
    video.each(function(i, el) {
      el.pause();
    });
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(() => {
      video.each(function(i, el) {
        el.play();
      });
    }, 300);
    return resizeTimer;
  };

  Drupal.shuryVideo.proportionCheck = function(video) {
    if (window.innerHeight / window.innerWidth <= 0.5625) {
      video.addClass('proportion');
    }
    else {
      video.removeClass('proportion');
    }
  };

})(jQuery, Drupal);
