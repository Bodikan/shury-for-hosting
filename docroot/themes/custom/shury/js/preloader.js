(function($, Drupal) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function(context, settings) {
      $('#status').fadeOut(350);
      $('#preloader').delay(350).fadeOut(200);
    },
  };
})(jQuery, Drupal);
