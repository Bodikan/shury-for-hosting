(function($, Drupal) {
  "use strict";

  Drupal.behaviors.mainMenu = {
    attach: function(context, settings) {
      $('#menu-button, #menu-close-button, #main-nav a').once('menu-button').on('click', function(e) {
        if ($('body').hasClass('front-page-true')){
          e.preventDefault();
        }
        $('body').toggleClass('pushed-left');
        $('#menu-button').toggleClass('open');

      });
    },
  };

  Drupal.behaviors.panelHeading = {
    attach: function(context, settings) {
      $('.panel-heading a').once('panel-heading').on('click', function() {
        $('.panel-heading').removeClass('active');
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in')) {
          $(this).parents('.panel-heading').addClass('active');
        }
      });
    },
  };


  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('body').addClass('touch');
  }

  Drupal.behaviors.mainNavMobile = {
    attach: function(context, settings) {
      $('#main-nav a').not('.sub-nav a').once('mainNavMobile').on('click touchend', function() {
        $('.sub-nav').stop().slideUp(420, function() {
          $(this).addClass('hidden');
        });
      });

      $('.sub-nav-toggle').once('mainNavMobileSub-nav-toggle').on('click touchend', function(e) {
        e.preventDefault();
        var $subNav = $(this).next('.sub-nav');
        if ($subNav.hasClass('hidden')) {
          $subNav.hide().removeClass('hidden').stop().slideDown(300);
        } else {
          $subNav.stop().slideUp(300, function() {
            $(this).addClass('hidden');
          });
        }
      });

    },
  };

  Drupal.behaviors.shuryContent = {
    attach: function(context, settings) {
      $('#content').once('mainNavMobileSubshuryContent').on('click', function() {
        $('.sub-nav').stop().fadeOut(420, function() {
          $(this).addClass('hidden');
        });
      });
    },
  };


  Drupal.behaviors.accordion = {
    attach: function(context, settings) {
      $('#accordion').once('accordion').on('shown.bs.collapse', function(e) {
        e.preventDefault();
        var offset = $('.panel.panel-default > .panel-collapse.in').offset();
        if (offset) {
          $('html,body').animate({
            scrollTop: $('.panel-collapse.in').offset().top - 120
          }, 500);
        }
      });
    },
  };

  Drupal.behaviors.bannerTextBlock = {
    attach: function(context, settings) {
      if ($('.banner-textblock, .custom-caption').length) {
        $(window).scroll(function() {
          var scrollPos = $(this).scrollTop();
          $('.banner-textblock').not('.touch .banner-textblock').css({
            'top': ((scrollPos / 5.0)) + "px",
            'opacity': 1 - (scrollPos / 550)
          });
          $('.custom-caption').not('.touch .custom-caption').css({
            'bottom': (160-(scrollPos / 5.0)) + "px",
            'opacity': 1 - (scrollPos / 550)
          });
          $('.parallax-banner').not('.touch .parallax-banner').css({
            'transform': 'translate3d(' + "0px" + ", " + ((-scrollPos / 9.0) + "px") + ", " + "0px" + ')'
          });
        });
      }
    },
  };

  Drupal.behaviors.scroller = {
    attach: function(context, settings) {
      $('a.scroller').once('scroller').on('click', function() {
        var the_id = $(this).get(0).hash;
        if ($(the_id).length > 0) {
          $('html, body').animate(
            {
              scrollTop: $(the_id).offset().top + 1
            }, 'slow');
          return false;
        }
      });
    },
  };

  Drupal.behaviors.Animatedblock = {
    attach: function(context, settings) {
      // Animatedblock
      var $animation_elements = $('.animatedblock');

      function check_if_in_view() {
        var window_height = $(window).height();
        var window_top_position = $(window).scrollTop();
        var window_bottom_position = (window_top_position + window_height);

        $.each($animation_elements, function() {
          var $element = $(this);
          var element_height = $element.outerHeight();
          var element_top_position = $element.offset().top;
          var element_bottom_position = (element_top_position + element_height);

          //check to see if this current container is within viewport
          if ((element_bottom_position >= window_top_position) &&
            (element_top_position <= window_bottom_position)) {
            $element.addClass('in-view');
          } else {
            //  $element.removeClass('in-view');
          }
        });
      }

      $(window).on('scroll resize', check_if_in_view);
      $(window).trigger('scroll');
    },
  };

})(jQuery, Drupal);
