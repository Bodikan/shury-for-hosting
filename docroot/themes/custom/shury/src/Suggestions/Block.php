<?php

namespace Drupal\shury\Suggestions;

/**
 * Class for code organization: Block Suggestions Alter.
 *
 * @package Drupal\monroe\Suggestions
 */
class Block {

  /**
   * Theme Suggestions View Alter.
   *
   * @param array &$suggestions
   *   Suggestions array.
   * @param array $variables
   *   Variables array.
   */
  public function alter(array &$suggestions, array $variables) {
    $elements = $variables['elements'];
    if (isset($elements['content']['#block_content'])) {
      /** @var \Drupal\block_content\Entity\BlockContent $block_content */
      $block_content = $elements['content']['#block_content'];
      $new_suggestions = [
        [
          'block',
          $block_content->getEntityTypeId(),
          $block_content->bundle(),
        ],
        [
          'block',
          $block_content->bundle(),
        ],
      ];
      foreach ($new_suggestions as $suggestion) {
        $suggestions[] = implode('__', $suggestion);
      }
    }
  }

}
