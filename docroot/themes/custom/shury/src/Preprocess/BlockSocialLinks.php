<?php

namespace Drupal\shury\Preprocess;

use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Url;

/**
 * Class for code organization: Preprocess SocialLinks block.
 *
 * @package Drupal\shury\Preprocess
 */
class BlockSocialLinks {

  /**
   * Preprocess the SocialLinks block.
   *
   * @param array $vars
   *   Variables array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function preprocess(array &$vars) {
    if (isset($vars['elements']['content']['#block_content'])) {
      /** @var \Drupal\block_content\Entity\BlockContent $block */
      $block = $vars['elements']['content']['#block_content'];

      $this->getSocials($block, $vars);
    }
  }

  /**
   * Assembles an array of social links.
   *
   * @param \Drupal\block_content\Entity\BlockContent $block_content
   *   The BlockContent entity.
   * @param array $variables
   *   The data array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function getSocials(BlockContent $block_content, array &$variables) {
    $social_items_field = 'field_paragraph_items';
    if ($block_content->hasField($social_items_field) && !$block_content->get($social_items_field)
      ->isEmpty()) {
      /** @var \Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList $social_items */
      $social_items = $block_content->get($social_items_field);
      /** @var \Drupal\entity_reference_revisions\Plugin\Field\FieldType\EntityReferenceRevisionsItem $item */
      foreach ($social_items as $item) {
        /** @var \Drupal\paragraphs\Entity\Paragraph $social_item */
        $social_item = $item->entity;
        if ($social_item->hasField('field_link') && $social_item->hasField('field_social_link_type')) {
          $link = $social_item->get('field_link')->first()->getValue();
          $link_type = $social_item->get('field_social_link_type')
            ->first()
            ->getValue()['value'];
          $variables['fields']['socials'][] = [
            'url' => $link['uri'],
            'class' => $link_type != 'personal_site' ? $link_type : 'internet-explorer',
          ];
        }
      }
    }
  }

}
