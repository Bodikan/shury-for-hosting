<?php

namespace Drupal\shury\Preprocess;

use Drupal\block_content\BlockContentInterface;
use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Url;

/**
 * Class for code organization: Html attachments.
 *
 * @package Drupal\shury\Preprocess
 */
class Html {

  /**
   * Preprocess the Html attachments.
   *
   * @param array $vars
   *   Variables array.
   */
  public function preprocess(array &$attachments) {

    $viewport = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'viewport',
        'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1',
      ],
    ];

    $attachments['#attached']['html_head'][] = [$viewport, 'viewport'];

    $format_detection = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'format-detection',
        'content' => 'telephone=no',
      ],
    ];

    $attachments['#attached']['html_head'][] = [$format_detection, 'format-detection'];

    $xuacompatible = [
      '#tag' => 'meta',
      '#attributes' => [
        'http-equiv' => 'x-ua-compatible',
        'content' => 'ie=edge',
      ],
    ];

    $variables['page']['#attached']['html_head'][] = [$xuacompatible, 'x-ua-compatible'];
  }

}
