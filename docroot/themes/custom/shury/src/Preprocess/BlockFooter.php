<?php

namespace Drupal\shury\Preprocess;

use Drupal\block_content\BlockContentInterface;
use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Url;

/**
 * Class for code organization: Preprocess Footer block.
 *
 * @package Drupal\shury\Preprocess
 */
class BlockFooter {

  /**
   * Preprocess the Footer block.
   *
   * @param array $vars
   *   Variables array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function preprocess(array &$vars) {
    $element = $vars['elements'];
    if ((isset($element['#id'])) && $element['#id'] === 'footer') {
      if (isset($element['content']['#block_content'])) {
        /** @var \Drupal\block_content\Entity\BlockContent $block */
        $block = $element['content']['#block_content'];
        if ($block instanceof BlockContentInterface) {
          $this->getTitle($block, $vars);
          $this->getDescription($block, $vars);
          $this->getSocials($block, $vars);
          $this->getCopyright($block, $vars);
          $this->getMenu($block, $vars);
          $this->isCreatedBy($block, $vars);
        }
      }
    }
  }

  /**
   * Returns Title field value.
   *
   * @param \Drupal\block_content\Entity\BlockContent $block_content
   *   The BlockContent entity.
   * @param array $variables
   *   The data array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function getTitle(BlockContent $block_content, array &$variables) {
    $field_name = 'field_title';
    if ($block_content->hasField($field_name)) {
      $variables['fields']['title'] = $block_content->get($field_name)->first()->getValue()['value'];
    }
  }

  /**
   * Returns Description field value.
   *
   * @param \Drupal\block_content\Entity\BlockContent $block_content
   *   The BlockContent entity.
   * @param array $variables
   *   The data array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function getDescription(BlockContent $block_content, array &$variables) {
    $field_name = 'body';
    if ($block_content->hasField($field_name)) {
      $variables['fields']['description'] = $block_content->get($field_name)->first()->getValue()['value'];
    }
  }

  /**
   * Assembles an array of social links.
   *
   * @param \Drupal\block_content\Entity\BlockContent $block_content
   *   The BlockContent entity.
   * @param array $variables
   *   The data array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function getSocials(BlockContent $block_content, array &$variables) {
    $field_name = 'field_social_links';
    if ($block_content->hasField($field_name)) {
      $link_attrs = [];
      /** @var \Drupal\link\Plugin\Field\FieldType\LinkItem $field_item */
      $social_field_items = $block_content->get($field_name);
      foreach ($social_field_items as $social_field_item) {
        $options = $social_field_item->getProperties()['options']->getValue();
        if (isset($options['attributes'])) {
          foreach ($options['attributes'] as $attribute => $value) {
            if (is_array($value)) {
              $value = implode(' ', $value);
            }
            $link_attrs[str_replace('-', '_', $attribute)] = $value;
          }
        }
        $variables['fields']['socials'][] = [
          'url'        => $social_field_item->getUrl()->toString(),
          'title'      => $social_field_item->title,
          'isExternal' => $social_field_item->getUrl()->isExternal(),
          'attributes' => $link_attrs,
        ];
      }
    }
  }

  /**
   * Returns Copyright field value.
   *
   * @param \Drupal\block_content\Entity\BlockContent $block_content
   *   The BlockContent entity.
   * @param array $variables
   *   The data array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function getCopyright(BlockContent $block_content, array &$variables) {
    $field_name = 'field_copyright';
    if ($block_content->hasField($field_name)) {
      $variables['fields']['copyright'] = $block_content->get($field_name)->first()->getValue()['value'];
    }
  }

  /**
   * Assembles an array of menu links.
   *
   * @param \Drupal\block_content\Entity\BlockContent $block_content
   *   The BlockContent entity.
   * @param array $variables
   *   The data array.
   */
  private function getMenu(BlockContent $block_content, array &$variables) {
    $field_name = 'field_footer_links';
    if ($block_content->hasField($field_name)) {
      $links = $block_content->get($field_name)->getValue();
      foreach ($links as $link) {
        $variables['fields']['menu'][] = [
          'title' => $link['title'],
          'url'   => Url::fromUri($link['uri'])->toString(),
        ];
      }
    }
  }

  /**
   * Checks to show the Created By text.
   *
   * @param \Drupal\block_content\Entity\BlockContent $block_content
   *   The BlockContent entity.
   * @param array $variables
   *   The data array.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function isCreatedBy(BlockContent $block_content, array &$variables) {
    $field_name = 'field_hide_created_by';
    if ($block_content->hasField($field_name)) {
      $variables['fields']['hide_created_by'] = $block_content->get($field_name)->first()->getValue()['value'];
    }
  }

}
