<?php

namespace Drupal\entity_embed\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginCssInterface;
use Drupal\Core\Url;
use Drupal\editor\Entity\Editor;
use Drupal\embed\EmbedButtonInterface;
use Drupal\embed\EmbedCKEditorPluginBase;
use Drupal\embed\Entity\EmbedButton;
use Drupal\media\Entity\MediaType;

/**
 * Defines the "drupalentity" plugin.
 *
 * @CKEditorPlugin(
 *   id = "drupalentity",
 *   label = @Translation("Entity"),
 *   embed_type_id = "entity"
 * )
 */
class DrupalEntity extends EmbedCKEditorPluginBase implements CKEditorPluginCssInterface {

  const OPENER_ID="editor_entity_embed";

  /**
   * {@inheritdoc}
   */
  protected function getButton(EmbedButtonInterface $embed_button) {
    $button = parent::getButton($embed_button);
    $button['entity_type'] = $embed_button->getTypeSetting('entity_type');
    return $button;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'entity_embed') . '/js/plugins/drupalentity/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/jquery',
      'core/drupal',
      'core/drupal.ajax',
      'entity_embed/media_library.dialog',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $buttons = $this->getButtons();
    $config = [
      'DrupalEntity_dialogTitleAdd' => t('Insert entity'),
      'DrupalEntity_dialogTitleEdit' => t('Edit entity'),
      'DrupalEntity_buttons' => $buttons,
    ];
    if (\Drupal::moduleHandler()->moduleExists('media_library') && class_exists(\Drupal\media_library\MediaLibraryUiBuilder::class) && array_key_exists('media', $buttons)) {
      $media_button = EmbedButton::load('media');
      assert($media_button instanceof EmbedButtonInterface);

      $valid_media_type_ids = array_keys(MediaType::loadMultiple());
      $embed_button_enabled_bundles = $media_button->getTypeSetting('bundles', []);
      $allowed_media_type_ids = empty($embed_button_enabled_bundles)
        ? $valid_media_type_ids
        : array_intersect($embed_button_enabled_bundles, $valid_media_type_ids);

      $state = \Drupal\media_library\MediaLibraryState::create(
        static::OPENER_ID,
        $allowed_media_type_ids,
        in_array('image', $allowed_media_type_ids, TRUE) ? 'image' : reset($allowed_media_type_ids),
        1
      );
      $config['DrupalEntity_mediaLibraryUrl'] = Url::fromRoute(
        'media_library.ui',
        [],
        ['query' => $state->all()]
      )->toString(TRUE)->getGeneratedUrl();
      $config['DrupalEntity_mediaLibraryDialogOptions'] = \Drupal\media_library\MediaLibraryUiBuilder::dialogOptions();
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getCssFiles(Editor $editor) {
    return [
      drupal_get_path('module', 'system') . '/css/components/hidden.module.css',
      drupal_get_path('module', 'entity_embed') . '/css/entity_embed.editor.css',
    ];
  }

}
