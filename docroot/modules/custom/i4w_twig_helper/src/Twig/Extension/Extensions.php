<?php

namespace Drupal\i4w_twig_helper\Twig\Extension;

/**
 * Custom twig extensions.
 */
class Extensions extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    $filters = [];

    $filters[] = new \Twig_SimpleFilter('nl2br2', [
      $this,
      'nl2br2',
    ]);

    return $filters;
  }

  /**
   * Returns the string with replaced \r\n.
   *
   * @param string $string
   *   Text string.
   *
   * @return mixed
   *   Replaced string.
   */
  function nl2br2($string) {
    $to_replace = [
      "\r\n\r\n",
      "\r\n",
      "\r",
      "\n",
    ];
    return str_replace($to_replace, "<br />", $string);
  }

}
