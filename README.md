# Install site

1. `$ fin start`
2. create and modify `settings.local.php`
3. import db or install site from existing configuration. 

# Use ES6 for custom JavaScript development

1. Node.js - ensure that you are using at least the latest long-term support 
(LTS) release of Node.js, which is available at the Node.js downloads page.
Alternatively, you can install Node.js via a package manager on many OS platforms.
If you need specific version of Node.js - use [nvm](https://github.com/nvm-sh/nvm).

2. Install Yarn using NPM:
`npm install -g yarn`

3. In terminal navigate to site root, `cd path/to/your/site` 

4. install package dependencies - `yarn install`

## Common Tasks
`yarn build:js` - build the prod version of the file.

`yarn build:js-dev` - build the dev version of the file with mapping.

`yarn watch:js` - watch files changes. Only `*.es6.js` files watched.
 
`yarn watch:js-dev` - - watch files changes. Only `*.es6.js` files watched.

### Some examples:
Build single file:

`yarn build:js-dev --file docroot/modules/custom/uvab_reports/js/reports.es6.js`

Watch directory:

`yarn watch:js-dev --dir docroot/modules/custom/uvab_reports/js/`

See more tasks in `package.json`.

### Resulting files:
The resulting file name will always be end with `.legacy.json`. 
So if original file name is `example.es6.js` then you must include in Drupal 
library `example.legacy.json`.

### Polyfill 
If you want to use ES6 method like .map, assigne etc.: 
Add the dependency to your .library.yml file: 
    - shury_core/polyfill



